﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SweeftDigitalTasks
{
    public static class ArrayNotContainsTask
    {
        /// <summary>
        /// 0 ზე მაღალი ისეთი მინიმალური რიცხვის ძებნა,
        /// რომელსაც მოცემული მასივი არ შეიცავს
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int NotContains(int[] array)
        {
            try
            {
                if (array.Length == 0 || array.Length == 1 && array[0] > 1) return 1;
                if (array.Length == 1 && array[0] == 1) return ++array[0];

                var result = 1;

                var found = true;

                for (int i = 0; i <= array.Length - 1; i++)
                {
                    for (int j = 0; j <= array.Length - 1; j++)
                        if (array[j] == result)
                        {
                            found = false;
                            break;
                        }

                    if (found) break;

                    found = true;
                    result++;
                }

                return result;
            }
            catch { throw; }
        }
    }
}
