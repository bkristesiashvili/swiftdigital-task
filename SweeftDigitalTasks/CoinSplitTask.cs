﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SweeftDigitalTasks
{
    public static class CoinSplitTask
    {
        /// <summary>
        /// ინდექსის განსაზღვრა რეკურსიული მეთოდის გარეთ
        /// </summary>
        private static int index = 0;

        /// <summary>
        /// ჩვენს ხელთ არსებული მონეტების მასივი
        /// </summary>
        private static readonly int[] COINS = { 50, 20, 10, 5, 1 };

        /// <summary>
        /// მინიმალური მონეტების რაოდენობის დაბრუნების მეთოდი
        /// </summary>
        /// <param name="coinAmount"></param>
        /// <returns></returns>
        public static int MinSplit(int coinAmount)
        {
            try
            {
                var coinsCount = 0;

                for (int i = 0; i < COINS.Length; i++)
                {
                    while(coinAmount >= COINS[i])
                    {
                        coinsCount++;
                        coinAmount -= COINS[i];
                    }
                }

                return coinsCount;
            }
            catch { throw; }
        }

        /// <summary>
        /// მინიმალური მონეტების რაოდენობის დაბრუნება რეკურსიული მეთოდით
        /// (ერთდროულად მომაფიქრდა ამ ამოცანის ასეთი SOLUTION და ორივე დავწერე)
        /// </summary>
        /// <param name="coinAmount"></param>
        /// <returns></returns>
        public static int MinSplitWithRecursion(int coinAmount)
        {
            try
            {
                if (coinAmount == 0) return 0;

                var count = 0;

                while(coinAmount >= COINS[index])
                {
                    coinAmount -= COINS[index];
                    count++;
                }

                index++;

                count += MinSplitWithRecursion(coinAmount);

                return count;
            }
            catch { throw; }
        }
    }
}
