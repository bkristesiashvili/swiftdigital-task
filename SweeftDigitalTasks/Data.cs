using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SweeftDigitalTasks
{
    public class Data<T> : IEnumerable
        where T : struct
    {
        private ArrayList items;
        private IDictionary<T, int> locations;

        /// <summary>
        /// ქმნის Data ონიექტს
        /// </summary>
        public Data()
        {
            items = new ArrayList();
            locations = new Dictionary<T, int>();
        }

        /// <summary>
        /// ინდექსატორი 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T this[int index] { get => (T)items[index]; }

        /// <summary>
        /// ელემენტის ჩამატება მონაცემტა სტრუქტურაში
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Insert(T value)
        {
            try
            {
                if (locations.ContainsKey(value)) return false;

                items.Add(value);
                locations.Add(value, items.Count - 1);

                return true;
            }
            catch { throw; }
        }

        /// <summary>
        /// რამდენიმე ელემენტის ერთდროულად ჩამატება
        /// </summary>
        /// <param name="array"></param>
        public void InsertRange(T[] array)
        {
            try
            {
                foreach (var item in array)
                    Insert(item);
            }
            catch { throw; }
        }

        /// <summary>
        /// ელემენტის წაშლა მონაცემთა სტრუქტურიდან
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Delete(T value)
        {
            try
            {
                if (!locations.ContainsKey(value)) return false;

                var index = locations[value];

                locations.Remove(value);

                if (index != items.Count - 1)
                {
                    items[index] = items[items.Count - 1];

                    locations.Remove((T)items[items.Count - 1]);

                    locations.Add((T)items[index], index);
                }

                items.RemoveAt(items.Count - 1);

                /**
                items.RemoveAt(index);

                locations.Clear();

                for (int i = 0; i <= items.Count - 1; i++)
                    locations[(T)items[i]] = i;
                */
                return true;
            }
            catch { throw; }
        }

        /// <summary>
        /// იმპლემენტაცია
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return items.GetEnumerator();
        }

        /// <summary>
        /// აბრუნებს მონაცემების მასივების რაოდენობას
        /// </summary>
        /// <returns></returns>
        public int Count() => items.Count;
    }
}
