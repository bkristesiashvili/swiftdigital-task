﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SweeftDigitalTasks
{
    public static class PalindromeTask
    {
        /// <summary>
        /// პალინდორიმის ამოცანის ამოხსნის ერთ-ერთი გზა
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsPalindrome(string input)
        {
            try
            {
                var toCharArray = input.ToCharArray();

                Array.Reverse(toCharArray);

                return input == string.Join("", toCharArray);
            }
            catch { throw; }
        }
    }
}
