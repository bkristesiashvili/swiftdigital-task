﻿using static System.Console;
using static SweeftDigitalTasks.PalindromeTask;
using static SweeftDigitalTasks.CoinSplitTask;
using static SweeftDigitalTasks.ArrayNotContainsTask;
using static SweeftDigitalTasks.ProperlyRoundBracketTask;
using static SweeftDigitalTasks.StairStepsTask;
using System;

namespace SweeftDigitalTasks
{
    public static class Program
    {
        static void Main(string[] args)
        {
            string palindrome = "1221";

            int amount = 16;

            int[] test = { 1, 2, 3, 6, 7, 8, 5, 10, 11 };

            var printArray = "";

            var properlyBracketsInput = ")(()(())())(())";

            var stairs = 5;

            Data<int> list = new Data<int>();

            var rssTask = new RssTask();

            var printStructureList = "";

            list.InsertRange(new[] { 1, 2, 4, 3, 5, 7, 6, 9 });

            foreach (var item in list)
                printStructureList += $"{ item },";

            foreach (var num in test)
                printArray += $"{ num },";

            try
            {

                #region PALINDROME TASK

                WriteLine("(1) #============= PALINDROME TASK ============== #");

                WriteLine($" # Is the Word \"{ palindrome }\" Palindrome? { IsPalindrome(palindrome) }\n");

                #endregion

                #region COIN SPLIT TASK

                WriteLine("(2) #============= COIN SPLIT TASK ============== #");

                WriteLine($" # Coin Amount: { amount }\n");

                WriteLine($" # Min Coin Split with Direct Method: { MinSplit(amount) }\n");

                WriteLine($" # Min Coin Split with Recursion Method: { MinSplitWithRecursion(amount) }\n");

                #endregion

                #region MIN NUMBER SEARCH TASK

                WriteLine($"(3) #============= MIN NUMBER SEARCH TASK ============== #");

                WriteLine($" # Input Array: { printArray.Remove(printArray.Length - 1, 1) }\n");

                WriteLine($" # Min Positive Number that isn't included in the array: { NotContains(test) }\n");

                #endregion

                #region PROPERLY ROUNDED BRACKETS TASK

                WriteLine($"(4) #============= PROPERLY ROUND BRACKETS TASK =============#");

                WriteLine($" Input string is: { properlyBracketsInput }\n");

                WriteLine($" # Is Properly inputed round brackets ( Search using REGEX)? { IsProperly(properlyBracketsInput) }");

                WriteLine($" # Is Properly inputed round brackets? { IsProperly2(properlyBracketsInput) }\n");

                #endregion

                #region STAIRS TASK

                WriteLine($"(5) #============= STAIRS VARIANTS TASK =============#");

                WriteLine($" # Stairs Count: { stairs }\n");

                WriteLine($" # Variants Count: { VariantsCount(stairs) }\n");

                #endregion

                #region DATA STRUCTURE TASK

                WriteLine($"(6) #============= CUSTOM DATA STRUCTURE DELETE O(1) TASK =============#");



                WriteLine($" # Input list : { printStructureList.Remove(printStructureList.Length - 1, 1) }\n");

                WriteLine($" # Deletable Item(s): { 2 },{ 7 }\n");
                list.Delete(2);
                list.Delete(7);

                printStructureList = string.Empty;

                if (list.Count() > 0)
                {
                    foreach (var item in list)
                        printStructureList += $"{ item },";

                    printStructureList = printStructureList.Remove(printStructureList.Length - 1, 1);
                }
                else
                {
                    printStructureList = "EMPTY"!;
                }

                WriteLine($" # After Deletion: : { printStructureList }\n");

                #endregion

                #region RSS TASK

                WriteLine($"(6) #============= RSS CURRENCY CONVERSION TASK =============#");

                WriteLine(rssTask);

                WriteLine($" # Conversion rate From [GEL] To [RUB]\n");

                WriteLine($" # 1 GEL = { rssTask.ExchangeRate("GEL", "RUB") } RUB\n");

                #endregion

                ReadKey();
            }
            catch(Exception e)
            {
                WriteLine(e.Message);
                ReadKey();
            }
        }
    }
}
