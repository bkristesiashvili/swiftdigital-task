﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SweeftDigitalTasks
{
    public static class ProperlyRoundBracketTask
    {
        /// <summary>
        /// REGEX პატერნით გაფილტვრა
        /// </summary>
        private const string PATTERN = @"\((?>\((?<c>)|[^()]+|\)(?<-c>))*(?(c)(?!))\)";

        /// <summary>
        /// მეთოდი REGEX-ის გამოყენებით ფილტრავს შეტანილ მონაცემს
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsProperly(string input)
        {
            try
            {
                input = Regex.Replace(input, PATTERN, string.Empty);

                if (input.Contains('(') || input.Contains(')'))
                    return false;
                return true;
            }
            catch { throw; }
        }

        /// <summary>
        /// ფრჩხილების გახსნა დახურვების რაოდენობის 
        /// დამთხვევით შემოწმება
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsProperly2(string input)
        {
            try
            {
                var open = 0;

                foreach (var symbol in input)
                {
                    if (open < 0) return false;
                    if (symbol == '(') open++;
                    else if (symbol == ')') open--;
                }

                return open == 0;
            }
            catch { throw; }
        }
    }
}
