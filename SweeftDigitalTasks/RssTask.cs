﻿using HtmlAgilityPack;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace SweeftDigitalTasks
{
    public sealed class RssTask
    {
        #region CONSTANTS

        /// <summary>
        /// საქართველოს ეროვნული ბანკსი გაცვლითი კურსების რესურსი
        /// </summary>
        private const string URL = "http://www.nbg.ge/rss.php";

        /// <summary>
        /// რესურსში საძებნი მშობელი TAG
        /// </summary>
        private const string TAG = "//tr";

        /// <summary>
        /// რესურსში საძებნი შვილობილი TAG-ის დასახელება
        /// </summary>
        private const string CHILD_TAG_NAME = "td";

        /// <summary>
        /// ნაციონალური ვალუტა
        /// </summary>
        private const double GEL = 1.0D;

        #endregion

        #region CONSTRUCTOR

        /// <summary>
        /// ქმნის RSS Task ობიექტს
        /// </summary>
        public RssTask()
        {
            var stream = GetXmlData(new Uri(URL));

            var data = GetDescriptionData(stream).ToString();

            Rates = GetRates(data).OrderBy(r => r.Name);
        }

        #endregion

        #region PROPERTIES

        /// <summary>
        /// აბრუნებს გაცვლითი კურსების ობიექტების მასივს
        /// </summary>
        public IEnumerable<ExchangeRate> Rates { get; }

        #endregion

        #region PUBLIC METHODS

        /// <summary>
        /// დაგვიბრუნებს გაცვლით კურსს
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public double ExchangeRate(string from, string to)
        {
            try
            {
                var rateFrom = 0.0D;
                var toRate = 0.0D;

                var fromCurrency = Rates.FirstOrDefault(r => r.Name.Equals(from));

                var toCurrency = Rates.FirstOrDefault(r => r.Name.Equals(to));

                rateFrom = from.ToLower().Equals("gel") ? GEL
                    : fromCurrency != null ? fromCurrency.Rate / fromCurrency.Count 
                    : throw new Exception($"Currency { from } not found!");

                toRate = to.ToLower().Equals("gel") ? GEL
                    : toCurrency != null ? toCurrency.Rate / toCurrency.Count
                    : throw new Exception($"Currency { to } not found!");

                return Math.Round(rateFrom / toRate, 4);
            }
            catch { throw; }
        }

        /// <summary>
        /// გადატვირთული მეთოდი 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder strBuilder = new StringBuilder();

            strBuilder.Append("----------------------------------------------------\n");
            strBuilder.Append($"| {"TICKER",-2} |   {"COUNT",-7}  |    {"RATE",-8}  |  {"CHANGED",-10} |\n");
            strBuilder.Append("----------------------------------------------------\n");

            foreach (var item in Rates)
                strBuilder.Append($"|  {item.Name,-2}   |  {item.Count,-7}   |  {item.Rate,-7} GEL |  {item.Changed:0.0000} GEL |\n");
            strBuilder.Append("----------------------------------------------------\n");

            return strBuilder.ToString();
        }

        #endregion

        #region PRIVATE METHODS

        /// <summary>
        /// მოაქვს მითითებული ვებ გვერდიდან Resource
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private Stream GetXmlData(Uri url)
        {
            try
            {
                if (url == null) throw new ArgumentNullException(nameof(url));
                if (string.IsNullOrEmpty(url.PathAndQuery)) throw new Exception("Invalid URL!");

                using var httpClient = new HttpClient();
                return httpClient.GetStreamAsync(url).Result;
            }
            catch { throw; }
        }

        /// <summary>
        /// გაფილტრავს XML დოკუმენტს და წამოიღებს აღწერის ველს
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private XElement GetDescriptionData(Stream stream)
        {
            try
            {
                var docment = XDocument.Load(stream);

                var data = from feed in docment.Descendants("item")
                           select new
                           {
                               Description = feed.Element("description")
                           };

                return data.FirstOrDefault()?.Description;
            }
            catch { throw; }
        }

        /// <summary>
        /// ამოღებულ გაცვლით კურსებს დააკონვერტირებს ობიექტების მასივად
        /// </summary>
        /// <param name="htmlData"></param>
        /// <returns></returns>
        private IEnumerable<ExchangeRate> GetRates(string htmlData)
        {
            try
            {
                var html = new HtmlDocument();

                html.LoadHtml(htmlData);

                int step = 0;

                var index = 0;

                var nodes = html.DocumentNode.SelectNodes(TAG);

                var list = new ExchangeRate[nodes.Count];

                foreach (HtmlNode node in nodes)
                {
                    var childs = node.ChildNodes;

                    list[index] = new ExchangeRate();

                    foreach (HtmlNode childNode in childs)
                    {
                        if (childNode.Name != CHILD_TAG_NAME) continue;
                        if (string.IsNullOrEmpty(childNode.InnerText) || string.IsNullOrWhiteSpace(childNode.InnerText)) continue;


                        if (step == 0) list[index].Name = childNode.InnerText;
                        if (step == 2) list[index].Rate = double.TryParse(childNode.InnerText, out var rate) ? rate : 0;
                        if (step == 3) list[index].Changed = double.TryParse(childNode.InnerText, out var changeRate) ? changeRate : 0;
                        if (step == 1)
                        {
                            var tempStr = Regex.Replace(childNode.InnerText, @"[^\d]", "");
                            list[index].Count = int.TryParse(tempStr, out var count) ? count : 1;
                        }

                        step++;

                        if (step > 3)
                        {
                            index++;
                            step = 0;
                            break;
                        }
                    }
                }

                return list;
            }
            catch { throw; }
        }

        #endregion
    }

    #region EXCHANGE RATE OBJECT

    /// <summary>
    /// გაცვლითი კურსის ობიექტის მოდელი
    /// </summary>
    public sealed class ExchangeRate
    {
        /// <summary>
        /// ვალუტის დასახელება
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// მიმდინარე გაცვლითი კურსი
        /// მითითებული რაოდენობის ღირებულება.
        /// </summary>
        public double Rate { get; set; }

        /// <summary>
        /// ვალუტის რაოდენობა
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// შეცვლილი პუნქტების რაოდენობა
        /// </summary>
        public double Changed { get; set; }
    }

    #endregion
}
