/**
 * ამოიღებს იმ მასწავლებლის სახელს და გვარს,
 * რომლის მოსწავლეც არის გიორგი
 */
select t.firstname as სახელი, t.lastname as გვარი, p.firstname as მოსწავლე from teachers_pupils tp 
inner join teacher t on t.id = tp.teacherid 
inner join pupil p on tp.pupilid = p.id where p.firstname = 'გიორგი'
