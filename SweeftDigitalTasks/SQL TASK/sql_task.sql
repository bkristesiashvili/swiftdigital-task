
/** USED DATABASE SYSTEM PL-SQL
 *  გამოყენებული მონაცემთა ბაზა არის PostgreSQL
 */
drop table if exists "teachers_pupils";
drop table if exists "teacher";
drop table if exists "pupil";
drop type if exists "sex" cascade;

/** CUSTOM SEX ENUME TYPE
 * შექმნის CUSTOME ENUM TYPE-ს 
 */
create type  Sex as enum('FEMALE','MALE');

/** TEACHER TABLE
 * შექმნის Teacher ცხრილს
 */
create table Teacher(
	id integer not null generated always as identity,
	firstname varchar(15) not null,
	lastname varchar(20) not null,
	sex Sex default('FEMALE'),
	subject varchar(25) not null,
	primary key(id)
);

/** PUPIL TABLE
 * შექმნის Pupil ცხრილს
 */
create table Pupil(
	id integer not null generated always as identity,
	firstname varchar(15) not null,
	lastname varchar(20) not null,
	sex Sex default('FEMALE'),
	"class" varchar(15) not null default('1 კლასი'),
	primary key(id)
);

/**
 * შექმნის MANY-TO-MANY რელაციას
 * Teacher და Pupil ცხრილებს შორის
 */
create table Teachers_Pupils(
	id integer not null generated always as identity,
	teacherId integer not null,
	pupilId integer not null,
	primary key(id),
	foreign key(teacherId) references Teacher(id) on delete cascade,
	foreign key(pupilId) references Pupil(id) on delete cascade
);

/**
 * ცასვამს ჩანაწერებს Teacher ცხრილში
 */
insert into teacher (firstname,lastname,sex,subject) values ('ბესიკ', 'ქრისტესიაშვილი', 'MALE','პროგრამირება');
insert into teacher (firstname,lastname,sex,subject) values ('მაია', 'გამყრელიძე', 'FEMALE','ისტორია');

/**
 * ჩასვამს ჩანაწერებს Pupil ცხრილში
 */
insert into pupil (firstname,lastname,sex,"class") values ('ვივიანა', 'ქრისტესიაშვილი', 'FEMALE','3 კლასი');
insert into pupil (firstname,lastname,sex,"class") values ('ალექსანდრე', 'ქრისტესიაშვილი', 'MALE','1 კლასი');
insert into pupil (firstname,lastname,sex,"class") values ('გიორგი', 'ქრისტესიაშვილი', 'MALE','1 კლასი');

/**
 *  მასწავლებლების და მოსწავლეების დაკავშირება
 */
insert into teachers_pupils (teacherid,pupilid) values (1, 1);
insert into teachers_pupils (teacherid,pupilid) values (1, 2);
insert into teachers_pupils (teacherid,pupilid) values (1, 3);
insert into teachers_pupils (teacherid,pupilid) values (2, 1);
insert into teachers_pupils (teacherid,pupilid) values (2, 2);
insert into teachers_pupils (teacherid,pupilid) values (2, 3);

