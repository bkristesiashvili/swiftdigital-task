﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SweeftDigitalTasks
{
    public static class StairStepsTask
    {
        /// <summary>
        /// საფეხურებზე ასვლის ვარიანტების რაოდენობის დათვლა
        /// (იგივეა რაც ფიბონაჩის მიმდევრობა)
        /// (0,1,1,2,3,5,8,13 ...)
        /// </summary>
        /// <param name="stairsCount"></param>
        /// <returns></returns>
        public static int VariantsCount(int stairsCount)
        {
            try
            {
                if (stairsCount == 1) return 1;
                if (stairsCount == 2) return 2;

                var n_1 = 1;
                var n = 2;

                var result = 0;

                for(int i = 2; i < stairsCount; i++)
                {
                    result = n_1 + n;

                    n_1 = n;
                    n = result;
                }

                return result;
            }
            catch { throw; }
        }
    }
}
